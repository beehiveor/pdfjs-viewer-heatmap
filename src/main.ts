/// <reference types="pdfjs-viewer-types/global" />
import "./global.css";
import type { DataPoint } from "heatmap.js";
import * as PDFh337 from "$lib";
import type { PDFHeatmapConfiguration } from "$lib";
import pdfSampleURL from "../assets/sample.pdf?url";
import type { EventBus } from "pdfjs-viewer-types";

if (!PDFViewerApplication) throw new Error("PDF Viewer is not loaded");

const valueDiv = document.querySelector<HTMLDivElement>("#value")!;

const app = PDFViewerApplication!;
let eventBus: EventBus;

type PageHeatmapInput = {
  id: number;
  min?: number;
  max?: number;
  config?: PDFHeatmapConfiguration,
  points: DataPoint<"w", "x", "y">[],
}

type HeatmapInput = {
  fingerprint?: string,
  config?: PDFHeatmapConfiguration;
  min?: number;
  max?: number;
  pages: PageHeatmapInput[],
}

let input: HeatmapInput | null = null;

Object.defineProperty(window, "inputData", {
  get() { return input }
})
Reflect.set(window, "PDFh337", PDFh337);

window.addEventListener("load", async () => {
  await Promise.all([
    app.open(pdfSampleURL),
    PDFh337.initPromise,
    loadSampleData(),
  ]);

  eventBus = app.eventBus as EventBus;

  eventBus.on("pagesloaded", () => input && setHeatmap(input));

  app.appConfig?.viewerContainer.addEventListener("drop", ondrop)

  app.appConfig?.viewerContainer.addEventListener("mousemove", onmousemove);
})

function onmousemove(event: MouseEvent) {
  const element = event.composedPath()
    .find(e => (e as Element).className === "page") as HTMLElement;
  let pageNumber = parseInt(element?.getAttribute("data-page-number") || "");
  if (!pageNumber) return;

  type View = { id: number, x: number, y: number };
  
  const views = (app.pdfViewer?._getVisiblePages() as any).views as View[];

  const view = views.find(v => v.id === pageNumber);

  if (!view) return;

  const container = PDFViewerApplication?.pdfViewer?.container!;

  const x = event.x - (view.x - container.scrollLeft + container.offsetLeft);
  const y = event.y - (view.y - container.scrollTop + container.offsetTop);

  const value = PDFh337.getValueAt(pageNumber, { x, y });

  valueDiv.style.visibility = "visible";
  valueDiv.style.left = event.clientX + "px";
  valueDiv.style.top = event.clientY + "px";
  valueDiv.innerText = value.toString();

  element.addEventListener("mouseleave", () => {
    valueDiv.style.visibility = "hidden";
  }, { once: true })
}

async function ondrop(event: DragEvent) {
  if (!event.dataTransfer) return;

  const file = Array
    .from(event.dataTransfer.files)
    .find(f => f.type === "application/json")

  if (file) {
    event.stopPropagation();
    event.preventDefault();

    input = JSON.parse(await file.text());
    input && setHeatmap(input);
  }
}

function setHeatmap(input: HeatmapInput) {
  if (input.fingerprint && input.fingerprint !== (app.store as any).fingerprint)
    return;
  PDFh337.cleanup();
  PDFh337.repaint();
  input.config && PDFh337.configure(input.config);
  input.min && PDFh337.setMinValue(input.min);
  input.max && PDFh337.setMinValue(input.max);

  input.pages.forEach(page => {
    page.config && PDFh337.configure(page.config, page.id);
    page.min && PDFh337.setMinValue(page.min, page.id);
    page.max && PDFh337.setMaxValue(page.max, page.id);
    PDFh337.setPoints(page.id, page.points);
  })
}

async function loadSampleData() {
  input = await import("../assets/sample.heatmap.json")
    .then(m => (m.default as unknown) as typeof input);
}
