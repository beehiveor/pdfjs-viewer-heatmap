import { resolve } from "path";
import { defineConfig } from "vite";
import { splitVendorChunkPlugin } from "vite";
import pdfViewer from "vite-plugin-pdfjs-viewer";

export default defineConfig(({ mode, command }) => {
  const pages = (command === "serve" || mode === "pages");
  const library = mode !== "pages";

  return {
    base: pages ? "/pdfjs-viewer-heatmap" : "/",
    resolve: {
      alias: {
        "$lib": resolve(__dirname, "lib"),
      },
    },
    build: library && {
      lib: {
        entry: resolve(__dirname, "lib/index.ts"),
        formats: ["umd"],
        name: "PDFh337",
        fileName: () => "heatmap.pdf.js",
      },
      /*
      // rollupOptions: {
      //   external: ["heatmap.js"],
      //   output: {
      //     globals: {
      //       "heatmap.js": "h337",
      //     }
      //   }
      // }
      */
    },
    plugins: [
      pages && pdfViewer({ index: false }),
      pages && splitVendorChunkPlugin(),
    ],
  }
})