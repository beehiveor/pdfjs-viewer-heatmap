import { PDFPageHeatmap } from "./pdfpageheatmap";
import type { DataPoint } from "@mars3d/heatmap.js";
import type {
  PDFHeatmapConfiguration,
} from "./pdfpageheatmap";
import type { EventBus } from "pdfjs-viewer-types";
import { PDFPageView } from "pdfjs-viewer-build";

export type { PDFHeatmapConfiguration } from "./pdfpageheatmap";
export { PDFPageHeatmap } from "./pdfpageheatmap";

let heatmaps: PDFPageHeatmap[] = [];

let globalConfig: PDFHeatmapConfiguration = {};

export const initPromise = PDFViewerApplication!.initializedPromise.then(() => {
  const eventBus = PDFViewerApplication!.eventBus as EventBus;
  eventBus.on("pagesloaded", () => {
    cleanup();
    heatmaps = [];
  })

  eventBus.on("pagerendered", ({ pageNumber }) => {
    const heatmap = getHeatmap(pageNumber);
    if (heatmap) heatmap.repaint(true);
  })

  const printContainer = PDFViewerApplication?.appConfig?.printContainer;

  if (!printContainer) return;

  const observer = new MutationObserver(mutations => {
    const image = mutations.at(0)?.addedNodes.item(0)?.firstChild as HTMLImageElement;
    if (!image || image.nodeName !== "IMG") return;
    const div = image.parentElement;

    const index = Array.from(printContainer.children).findIndex(c => c === div);
    const heatmapCanvas = getHeatmap(index + 1).repaint(true).getCanvas();

    if (!heatmapCanvas) return;

    const canvas = document.createElement("canvas");
    const ctx = canvas.getContext("2d");
    if (!ctx) return;

    image.addEventListener("load", () => {
      canvas.width = image.width;
      canvas.height = image.height;

      ctx.drawImage(image, 0, 0);
      ctx.globalAlpha = heatmapCanvas.getContext("2d")!.globalAlpha;
      ctx.drawImage(heatmapCanvas, 0, 0, canvas.width, canvas.height);
      image.src = ctx.canvas.toDataURL();
    }, { once: true })
  })
  observer.observe(printContainer, { childList: true });
})

if (!PDFViewerApplication)
  throw new Error("PDFViewerApplication is not loaded");

export function configure(cfg: PDFHeatmapConfiguration, pageNumber?: number) {
  if (pageNumber) getHeatmap(pageNumber)?.configure(cfg);
  else {
    globalConfig = cfg;
    heatmaps.forEach(h => h.configure(globalConfig));
  }
}

export function setPoints(pageNumber: number, points: DataPoint<"w">[]) {
  getHeatmap(pageNumber)?.setPoints(points);
}

export function setMaxValue(value: number, pageNumber?: number) {
  if (pageNumber) getHeatmap(pageNumber)?.setMaxValue(value);
  else {
    heatmaps.forEach(h => h.setMaxValue(value));
  }
}

export function setMinValue(value: number, pageNumber?: number) {
  if (pageNumber) getHeatmap(pageNumber)?.setMinValue(value);
  else {
    heatmaps.forEach(h => h.setMinValue(value));
  }
}

export function addPoint(pageNumber: number, point: DataPoint<"w">) {
  getHeatmap(pageNumber)?.addPoint(point);
}

export function repaint(pageNumber?: number) {
  if (pageNumber) getHeatmap(pageNumber).repaint();
  else heatmaps.forEach(h => h.repaint());
}

export function getValueAt(pageNumber: number, point: { x: number, y: number }) {
  return getHeatmap(pageNumber).getValueAt(point);
}

export function getValueAtScaled(pageNumber: number, point: { x: number, y: number }) {
  return getHeatmap(pageNumber).getValueAtScaled(point);
}

export function cleanup(pageNumber?: number) {
  if (pageNumber) getHeatmap(pageNumber).cleanup();
  else heatmaps.forEach(h => h.cleanup());
}

export function getHeatmap(pageNumber: number) {
  const index = pageNumber - 1;

  if (!heatmaps[index]) {
    const page = getPage(index)
    if (page)
      heatmaps[index] = new PDFPageHeatmap(page, globalConfig);
  }

  return heatmaps[index];
}

function getPage(index: number): PDFPageView {
  return PDFViewerApplication?.pdfViewer?.getPageView(index) as PDFPageView;
}
