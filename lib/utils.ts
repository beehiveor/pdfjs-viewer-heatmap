export function getCanvasWrapper(target: HTMLElement) {
  // const [ canvasWrapper ] = target.getElementsByClassName("canvasWrapper");
  // return canvasWrapper;
  return target.querySelector<HTMLDivElement>(".canvasWrapper");
}

export function getHeatmapCanvas(target: HTMLElement): HTMLCanvasElement {
  // const [ canvas ] = target.getElementsByClassName("heatmap-canvas");
  // return canvas;
  return target.querySelector<HTMLCanvasElement>(".heatmap-canvas");
}