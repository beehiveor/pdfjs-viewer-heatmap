/// <reference types="pdfjs-viewer-types/global" />

import { create } from "@mars3d/heatmap.js";
import type { HeatmapConfiguration, Heatmap, DataPoint } from "@mars3d/heatmap.js";
import type { PDFPageView } from "pdfjs-viewer-build";

export type PDFHeatmapConfiguration = Omit<
  HeatmapConfiguration<"w">,
  "container" | "xField" | "yField" | "valueField" | "scaleRadius" | "onExtremaChange"
>;

export class PDFPageHeatmap {
  public page: PDFPageView;
  private _heatmap: Heatmap<"w", "x", "y"> | null;
  private _config: PDFHeatmapConfiguration;

  private _points: DataPoint<"w">[];
  private _max: number;
  private _min: number;

  private _width: number;
  private _height: number;
  private _scale: number;

  constructor(page: PDFPageView, config?: PDFHeatmapConfiguration) {
    this.page = page;
    this._config = config || {};
    this._heatmap = null;

    this._points = [];
    this._min = 0;
    this._max = 1;

    this._width = page.width;
    this._height = page.height;
    this._scale = page.scale * 100;

    if (page.renderingState === 3) {
      this.repaint();
    }
  }

  public getPoints() {
    return [ ...this._points ];
  }

  private getContainer() {
    return this.page.div.querySelector<HTMLElement>(".canvasWrapper");
  }

  public repaint(force?: boolean) {
    const container = this.getContainer();

    this._width = this.page.width;
    this._height = this.page.height;
    this._scale = this.page.scale * 100;

    if (container && this._heatmap && !force) {
      this._heatmap.repaint();
    } else {
      const canvas = this.getCanvas(container);
      if (canvas) container?.removeChild(canvas);
      this._heatmap = null;

      if (container) {
        this._heatmap = create({
          container,
          ...this._config,
          radius: (this._config.radius || 1) * this._scale,
        })
      }

      if (this._points) {
        this.setPoints(this._points)
      }
    }

    return this;
  }

  public getCanvas(container?: HTMLElement | null): HTMLCanvasElement | null {
    return (container || this.getContainer())?.querySelector<HTMLCanvasElement>(".heatmap-canvas") || null;
  }

  public configure(config: PDFHeatmapConfiguration) {
    this._config = {...config };
    const container = this.getContainer();
    if (container) {
      return this._heatmap?.configure({
        container,
        ...this._config,
        radius: (this._config.radius || 1) * this._scale,
      });
    } else {
      return null;
    }
  }

  public addPoint(point: DataPoint<"w">) {
    this._points.push(point);

    if (this._heatmap) {
      this._heatmap.addData(this.scalePoint(point));
    }

    return this;
  }

  private scalePoint({ x, y, w }: DataPoint<"w">): DataPoint<"w"> {
    return {
      x: (x * this._width) >>> 0,
      y: (y * this._height) >>> 0,
      w,
    }
  }

  public setPoints(points: DataPoint<"w">[]) {
    this._points = points;

    if (this._heatmap) {
      this._heatmap.setData({
        min: this._min,
        max: this._max,
        data: points.map(p => this.scalePoint(p)),
      });
    }
    return this;
  }

  public setMaxValue(value: number) {
    this._max = value;
    this._heatmap?.setDataMax(value);
    return this;
  }

  public setMinValue(value: number) {
    this._min = value;
    this._heatmap?.setDataMin(value);
    return this;
  }

  public getValueAtScaled(point: { x: number, y: number }): number {
    return this.getValueAt({
      x: point.x * this._width,
      y: point.y * this._height,
    });
  }

  public getValueAt(point: { x: number, y: number }): number {
    const value = this._heatmap?.getValueAt(point);
    return value !== undefined ? value : NaN;
  }

  public cleanup() {
    const container = this.getContainer();
    const canvas = this.getCanvas();

    if (container && canvas) {
      container.removeChild(canvas);
    }
    this._heatmap = null;
    this._points = [];
    this._max = 1;
    this._min = 0;

    return this;
  }
}